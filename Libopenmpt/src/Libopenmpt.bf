using System;

namespace Libopenmpt;

public static class Libopenmpt
{
	[CRepr]
	public struct openmpt_module;

	public function void openmpt_log_func(uint8* message, void* user);

	public function void openmpt_error_func(int error, void* user);

	[CRepr]
	public struct openmpt_module_initial_ctl {
		char8* ctl;
		char8* value;
	}


	[CLink]
	public static extern openmpt_module* openmpt_module_create_from_memory2(void* filedata, int32 size,
		openmpt_log_func* logfunc, void* loguser, openmpt_error_func* errfunc, void* erruser, int32* error, char8* error_message, openmpt_module_initial_ctl* ctls);

	[CLink]
	public static extern int32 openmpt_module_read_stereo(openmpt_module* mod, int32 samplerate, int32 count, int16* left, int16* right);

	[CLink]
	public static extern int32 openmpt_module_read_interleaved_stereo(openmpt_module* mod, int32 samplerate, int32 count, int16* interleaved_stereo);

	[CLink]
	public static extern void openmpt_module_destroy(openmpt_module* mod);

	[CLink]
	public static extern int32 openmpt_module_ctl_set_text(openmpt_module* mod, char8* ctl, char8* value);
}