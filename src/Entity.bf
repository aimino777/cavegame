using SDL2;

namespace CaveGame;

class Entity
{
	public bool mIsDeleting;
	public int32 mCounter;
	public float mX;
	public float mY;
	public float mSpeedX = 0;
	public float mSpeedY = 0;
	public float mSizeX = 0;
	public float mSizeY = 0;
	
	public bool IsOffscreen(float marginX, float marginY)
	{
		return ((mX < -marginX) || (mX >= gGame.mWidth + marginX) ||
			(mY < -marginY) || (mY >= gGame.mHeight + marginY));
	}

	public virtual void Update()
	{
	}

	public virtual void Draw()
	{
	}
}
