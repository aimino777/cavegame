using Xml_Beef; // https://github.com/thibmo/Xml-Beef
// maybe this is better https://github.com/HorseTrain/Beef-Lang-XML
using System;
using System.Diagnostics;
using System.IO;
using System.Collections;
using SDL2;


namespace CaveGame;

class Tileset
{
	public int mSize;
	public int mWidth;
	public int mHeight;
	public int mFirstGid;
	public String mFileNameXml; //TODO: maybe delete this later
	public String mFileNameImage;
}

class Layer
{
	public int mWidth;
	public int mHeight;
	public List<int> mData;
}

class TiledMap
{
	public let mPathBase = "TiledMap/";
	public let mFileName = "ruins1.tmx";
	//TODO: how do I do 3d array?
	public int mWidth;
	public int mHeight;
	public List<Layer> mLayersData = new .();
	public List<Tileset> mTilesetData = new .();

	public Layer mSolid = null;
	public Layer mPlatforms = null;

	public this() {
		gMap = this;
	}

	public ~this() {
		for (let tileset in mTilesetData) {
			delete tileset.mFileNameXml;
			delete tileset.mFileNameImage;
			delete tileset;
		}
		for (let layer in mLayersData) {
			delete layer.mData;
			delete layer;
		}
		delete mTilesetData;
		delete mLayersData;
	}

	public void Parse() {

		let xml = scope Xml();
		xml.PreserveWhitespace = true;
		xml.LoadFromFile(scope $"{mPathBase}{mFileName}");
		let mapNode = xml.ChildNodes.FindNode("map");
		for (var node in mapNode.ChildNodes) {
			//Debug.WriteLine(node.Name);
			if (node.Name.Equals("tileset")) {
				Tileset tileset = new Tileset();
				for (var attr in node.AttributeList) {
					if (attr.Name.Equals("firstgid")) {
						tileset.mFirstGid = Int.Parse(attr.Value).Value;
						//Debug.WriteLine($"tileset.mFirstGid is {tileset.mFirstGid}");
					} else if (attr.Name.Equals("source")) {
						tileset.mFileNameXml = new String(attr.Value);
					}
				}
				ParseTileset(tileset);
				mTilesetData.Add(tileset);
			} else if (node.Name.Equals("layer")) {
				mLayersData.Add(ParseLayer(node));
			}
		}
	}

	void ParseTileset(Tileset tileset) {
		let xml = scope Xml();
		xml.PreserveWhitespace = true;
		xml.LoadFromFile(scope $"{mPathBase}{tileset.mFileNameXml}");
		let tilesetNode = xml.ChildNodes.Find("tileset");
		var tilecount = 0;
		var columns = 0;
		for (let attribute in tilesetNode.AttributeList) {
			if (attribute.Name.Equals("tilewidth")) {
				tileset.mSize = Int.Parse(attribute.Value);
			} else if (attribute.Name.Equals("tilecount")) {
				tilecount = Int.Parse(attribute.Value);
			} else if (attribute.Name.Equals("columns")) {
				columns = Int.Parse(attribute.Value);
			}
		}

		if (tilecount == 0 || columns == 0) Runtime.FatalError();

		tileset.mWidth = columns;
		tileset.mHeight = tilecount / columns;

		let imageNode = tilesetNode.ChildNodes.Find("image");
		for (let attribute in imageNode.AttributeList) {
			if (attribute.Name.Equals("source")) {
				tileset.mFileNameImage = new String(attribute.Value);
			}
		}

	}

	Layer ParseLayer(XmlNode layerNode) {
		var width = 0;
		var height = 0;
		var solid = false;
		var platforms = false;
		for (let attr in layerNode.AttributeList) {
			if (attr.Name.Equals("width")) {
				width = Int.Parse(attr.Value);
			} else if (attr.Name.Equals("height")) {
				height = Int.Parse(attr.Value);
			} else if (attr.Name.Equals("name")) {
				if (attr.Value.StartsWith("solid")) {
					solid = true;
				} else if (attr.Value.StartsWith("platforms")) {
					platforms = true;
				}

			}
		}
		let data = new List<int>(width*height);
		let dataNode = layerNode.ChildNodes.Find("data");

		let text = dataNode.Text;
		for (var s in text.Split(',')) {
			s.Trim();
			let res = Int.Parse(s);
			if (res case .Err) {
				Runtime.FatalError("Int.Parse error");
			}
			let num = res.Value;
			data.Add(num);
			//if (num != 0)
				//Debug.WriteLine(s);
		}

		var layer = new Layer();
		layer.mWidth = width;
		layer.mHeight = height;
		layer.mData = data;

		if (solid) {
			mSolid = layer;
		} else if (platforms) {
			mPlatforms = layer;
		}

		return layer;
	}

	public void Draw() {
		for (let layer in mLayersData) {
			for (let y in 0 ..< layer.mHeight) {
				for (let x in 0 ..< layer.mWidth) {
					let i = y * layer.mWidth + x;
					var tile = layer.mData[i];
					if (tile == 0) {
						continue;
					}
					DrawTile(tile, x, y);
				}
			}
		}
	}

	[Inline]
	void DrawTile(int tile, int x, int y) {
		let zoom = gGame.mZoom;

		var tile;
		tile--;
		for (let tileset in mTilesetData) {
			let tilecount = tileset.mWidth * tileset.mHeight;
			if (tile < tilecount) {
				let tilex = tile % tileset.mWidth;
				let tiley = tile / tileset.mWidth;

				int32 size = (.)mTilesetData[0].mSize;

				SDL.Rect srcRect = .((.)tilex*size, (.)tiley*size, size, size);
				SDL.Rect destRect = .((.)x*size*zoom, (.)y*size*zoom, size*zoom, size*zoom);
				destRect.x -= (.)gGame.mCameraX * zoom;
				destRect.y -= (.)gGame.mCameraY * zoom; //TODO: join zoom
				destRect.x += (.)gGame.mWidth / 2;
				destRect.y += (.)gGame.mHeight / 2;
				SDL.RenderCopy(gGame.mRenderer, Images.sMaps[@tileset.Index].mTexture, &srcRect, &destRect);
				break;
			}
			tile = tile - tilecount;
		}
	}

	public (int, int) GetCoordinates(float x, float y) {
		let size = mTilesetData[0].mSize;
		return ((.)Math.Floor(x / size), (.)Math.Floor(y / size));
	}

	public bool IsSolid(int x, int y) {
		var x, y;
		let layer = mSolid;
		if (x < 0 || y < 0 || x >= layer.mWidth || y >= layer.mHeight) return true;
		return layer.mData[y*layer.mWidth + x] != 0;
	}

	public bool IsPlatform(int x, int y) {
		var x, y;
		let layer = mPlatforms;
		if (x < 0 || y < 0 || x >= layer.mWidth || y >= layer.mHeight) return true;
		return layer.mData[y*layer.mWidth + x] != 0;
	}

	public bool IsSolidCoordinates(float x, float y) {
		let (ix, iy) = GetCoordinates(x, y);
		return IsSolid(ix, iy);
	}

	public bool IsPlatformCoordinates(float x, float y) {
		let (ix, iy) = GetCoordinates(x, y);
		return IsPlatform(ix, iy);
		//TODO: you get coordinates twice in some cases, can optimize
	}

	public (float, float) GetWorldCoordinates(int x, int y) {
		let size = (float)mTilesetData[0].mSize;
		return ((.)x * size + size / 2, (.)y * size + size / 2);
	}

	public bool DoesItCollide(Entity entity, float speedX, float speedY) {
		if (entity.mSizeX == 0) return false;

		let size = (float)mTilesetData[0].mSize;

		if (entity.mSizeX > size || entity.mSizeY > size) {
			Runtime.FatalError("not implemented");
		}

		let l = entity.mX + speedX;
		let r = l + entity.mSizeX;
		let t = entity.mY + speedY;
		let b = t + entity.mSizeY;
		if (IsSolidCoordinates(l,t)) return true;
		if (IsSolidCoordinates(l,b)) return true;
		if (IsSolidCoordinates(r,t)) return true;
		if (IsSolidCoordinates(r,b)) return true;
		let hero = entity as Hero;
		if (mPlatforms != null && entity.mSpeedY >= 0 &&
			hero.mJumpThrough != true &&
			!IsPlatformCoordinates(entity.mX, entity.mY) &&
			!IsPlatformCoordinates(entity.mX, entity.mY + entity.mSizeY) &&
			!IsPlatformCoordinates(entity.mX + entity.mSizeX, entity.mY) &&
			!IsPlatformCoordinates(entity.mX + entity.mSizeX, entity.mY + entity.mSizeY)
			) {
			if (IsPlatformCoordinates(l,t)) return true;
			if (IsPlatformCoordinates(l,b)) return true;
			if (IsPlatformCoordinates(r,t)) return true;
			if (IsPlatformCoordinates(r,b)) return true;
		}
		return false;
	}
}

static {
	public static TiledMap gMap;
}