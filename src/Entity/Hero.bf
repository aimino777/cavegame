using SDL2;
using System;
using System.Diagnostics;

namespace CaveGame;

class Hero : Entity
{
	public const int cShootDelay = 10; // How many frames to delay between shots
	public const float cMoveSpeed = 0.5f;

	public int mHealth = 1;
	public int mShootDelay;

	public int mReviveDelay = 0;
	public int mInvincibleDelay = 0;

	public let mDrawSize = (int32) 24;

	public let mSizeBottom = (float) 12;
	public let mSizeTop = (float) 5;
	public let mSizeSide = (float) 4;

	public let mDrawOffsetX = (int32)(mDrawSize / 2 - mSizeSide);
	public let mDrawOffsetY = (int32)(mDrawSize / 2 - mSizeTop);

	public var mTouchesBottom = true;
	public var mFacesRight = true;

	public var mSpeedMax = (float) 6;

	//public var mTouchesBottom = false;
	public var mTouchesTop = false;
	public var mTouchesLeft = false;
	public var mTouchesRight = false;

	public var mSpeedChange = false;
	public var mDash = true;

	public var mJumpThrough = false;

	private bool mDescends = false;

	private int32 mJumpAllowedCounter = 0;
	private readonly int32 mJumpAllowedMax = 28;

	public this() {
		mSizeX = (float) 4 * 2 - 1; //TODO: makes collision better, but I'm not sure if this is correct.
		mSizeY = (float) mSizeTop + mSizeBottom - 1;
	}



	public override void Draw()
	{
		if (mReviveDelay > 0)
			return;

		if ((mInvincibleDelay > 0) && ((mInvincibleDelay / 5 % 2 == 0)))
			return;

		let spriteX = (int32)mDrawSize;
		let spriteY = (int32)mDrawSize;

		let x = int32(mX);
		let y = int32(mY);
		let image = Images.sHero;
		let zoom = gGame.mZoom;

		SDL.Rect srcRect = .(0, 0, spriteX, spriteY);
		SDL.Rect destRect = .(x - mDrawOffsetX, y - mDrawOffsetY, spriteX, spriteY);
		destRect.x -= (.)gGame.mCameraX;
		destRect.y -= (.)gGame.mCameraY;

		destRect.x += (.)gGame.mWidth / 2 / zoom;
		destRect.y += (.)gGame.mHeight / 2 / zoom;

		destRect.x *= zoom;
		destRect.y *= zoom;
		destRect.w *= zoom;
		destRect.h *= zoom;

		//deciding sprite
		if (mTouchesBottom) {
			if (mSpeedX != 0) {
				let sprites = (int32)5;
				let stepLen = (int32)16;
				var absSpeed = (int32)mSpeedX;
				if (absSpeed < 0) absSpeed = - absSpeed;
				var c = mCounter;
				c += absSpeed;
				c = c % (sprites * stepLen);
				mCounter = c;
				let frame = c / stepLen;
				//Debug.WriteLine(scope $"{frame}");
				//srcRect.x = spriteX * (1 + frame);
				srcRect.x = spriteX * (1 + frame);
			} else {
				mCounter = 0;
			}
		} else { //mSpeedY == 0
			srcRect.y = spriteY;
			int32 frame;
			if (mSpeedY < -1) {
				frame = 0;
			} else if (mSpeedY > 1) {
				frame = 2;
			} else {
				frame = 1;
			}
			srcRect.x = spriteX * frame;
		}

		//SDL.RenderCopy(gGame.mRenderer, image.mTexture, &srcRect, &destRect);

		SDL.RendererFlip flip = .None;
		if (!mFacesRight) flip = .Horizontal;

		//SDL.RenderCopy(gGame.mRenderer, image.mTexture, &srcRect, &destRect);
		SDL.RenderCopyEx(gGame.mRenderer, image.mTexture, &srcRect, &destRect, 0, null, flip);
	}

	public override void Update()
	{
		if (mReviveDelay > 0)
		{
			if (--mReviveDelay == 0)
				gGame.mScore = 0;
			return;
		}

		if (mInvincibleDelay > 0)
			mInvincibleDelay--;

		base.Update();
		if (mShootDelay > 0)
			mShootDelay--;

		if (mHealth < 0)
		{
			gGame.ExplodeAt(mX, mY, 1.0f, 0.5f);
			gGame.PlaySound(Sounds.sExplode, 1.2f, 0.6f);
			gGame.mDifficulty = 0;

			mHealth = 1;
			mReviveDelay = 100;
			mInvincibleDelay = 100;
		}

		RunPhysicsControls();

		if (mSpeedY < 0) {
			mSpeedY = mSpeedY;
		}

		RunPhysics();

		if (mSpeedY == 0) mDescends = false;
		if (mDescends == false && mSpeedY > 0) {
			Debug.WriteLine(scope $"height is {mY}");
			mDescends = true;
		}

		gGame.mCameraX = (.)mX;
		gGame.mCameraY = (.)mY;

		if (mSpeedY != 0) Debug.WriteLine(scope $"{mSpeedY}");

	}

	void RunPhysics() {
		/*if (!gGame.mFly && !mTouchesBottom) {
			
			if (mSpeedY > mSpeedMax) mSpeedY = mSpeedMax;
		}*/

		mTouchesBottom = gMap.DoesItCollide(this, 0, 1) && (mSpeedY >= 0);
		if (!mTouchesBottom) {
			mSpeedY += 0.5f;
			if (mSpeedY > mSpeedMax) mSpeedY = mSpeedMax;
		} else {
			mY = Math.Floor(mY);
		}

		if (mSpeedX != 0) mFacesRight = mSpeedX > 0;

		//mX is 0.025502, mY is 199.7672, mSpeedX = 0.25, mSpeedY = 0.5
		//mX = 395.25f; mY = 102.5f; mSpeedX = -7f; mSpeedY = -6f;
		//Debug.WriteLine(scope $"mX = {mX}f;my = {mY}f; mSpeedX = {mSpeedX}f; mSpeedY = {mSpeedY}f;");
		RunPhysics2();
		//RunPhysicsRecursion();
		//mX = Math.Round(mX);
		//mY = Math.Round(mY);
	}

	void RunPhysics2() {

		if (gMap.DoesItCollide(this, mSpeedX, 0)) {
			while(!gMap.DoesItCollide(this, Math.Sign(mSpeedX), 0)) {
				mX += Math.Sign(mSpeedX);
			}
			mSpeedX = 0;
		}
		mX += mSpeedX;

		if (gMap.DoesItCollide(this, 0, mSpeedY)) {
			while(!gMap.DoesItCollide(this, 0, Math.Sign(mSpeedY))) {
				mY += Math.Sign(mSpeedY);
			}
			if (mSpeedY > 0) {
				mTouchesBottom = true;
			}
			mSpeedY = 0;
		}
		mY += mSpeedY;
	}

	private void RunPhysicsControls() {
		float deltaX = 0;
		float deltaY = 0;
		float moveSpeed = Hero.cMoveSpeed;
		float dashSpeed = 8.0f;

		mSpeedChange = false;

		if (gGame.mKeyDash) {
			mDash = true;
		} else {
			mDash = false;
		}


		if (mDash) {
			if (gGame.mKeyLeft) {
				deltaX -= dashSpeed;
			}
			if (gGame.mKeyRight) {
				deltaX += dashSpeed;
			}
		} else {

			if (mTouchesBottom) {
				var multiplier = (float) 16 ;
				if (mSpeedChange) {
					multiplier /= 2;
				}
				if (gGame.mKeyLeft) {
					deltaX -= moveSpeed * multiplier / 4;
				}
				if (gGame.mKeyRight) {
					deltaX += moveSpeed * multiplier / 4;
				}

				if (gGame.mKeyUp) {
					mJumpAllowedCounter = mJumpAllowedMax;
					if (mSpeedChange) {
						multiplier *= 2;
					}
					deltaY -= moveSpeed * multiplier * 2;
				}

			} else {
				if (gGame.mKeyLeft) {
					deltaX -= moveSpeed / 2;
				}
				if (gGame.mKeyRight) {
					deltaX += moveSpeed / 2;
				}

				if (gGame.mKeyUp) {
					if (mJumpAllowedCounter != 0) {
						mJumpAllowedCounter--;
						deltaY -= moveSpeed / 2;
					}
				}
				/*if (IsKeyDown(.Down) || IsKeyDown(.S)) {
					deltaY += moveSpeed / 2;
				}*/
			}

			
		}
		if (mDash) {
			mSpeedY = 0;
			if ((deltaX != 0)) {
				mSpeedX = deltaX;
			} else {
				mSpeedY = 0;
				if (mFacesRight) {
					mSpeedX = dashSpeed;
				} else {
					mSpeedX = -dashSpeed;
				}
			}
		} else {
			if ((deltaX != 0) || (deltaY != 0))
			//if (true)
			{
				//deltaX = -moveSpeed * 16;
				//deltaY = 0;
				mSpeedX += deltaX;
				mSpeedY += deltaY;

				var max = mSpeedMax;
				var vmax = max / 2;
				if (mSpeedX > vmax) mSpeedX = vmax;
				if (mSpeedX < -vmax) mSpeedX = -vmax;
				if (mSpeedY > max) mSpeedY = max;
				if (mSpeedY < -max) mSpeedY = -max;
			}
		}

		if (mTouchesBottom && mSpeedX != 0 && deltaX == 0) {
			var sx = mSpeedX;
			let sign = Math.Sign(sx);
			sx = Math.Abs(sx);
			sx -= Math.Min(sx, 1);
			sx *= sign;
			mSpeedX = sx;
		}

		mJumpThrough = false;
		if (mTouchesBottom) {
			if (gGame.mKeyUp && gGame.mKeyDown) {
				mJumpThrough = true;
			}
		}
	}
}
