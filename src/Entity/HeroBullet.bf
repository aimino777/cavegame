namespace CaveGame;

class HeroBullet : Entity
{
	public override void Update()
	{
		mY -= 8.0f;
		if (mY < -16)
			mIsDeleting = true;

		for (let entity in gGame.mEntities)
		{
			if (let enemy = entity as Enemy)
			{
				if ((enemy.mBoundingBox.Contains((.)(mX - entity.mX), (.)(mY - entity.mY))) && (enemy.mHealth > 0))
				{
					mIsDeleting = true;
					enemy.mHealth--;
					
					gGame.ExplodeAt(mX, mY, 0.25f, 1.25f);
					gGame.PlaySound(Sounds.sExplode, 0.5f, 1.5f);

					break;
				}
			}
		}
	}

	public override void Draw()
	{
		gGame.Draw(Images.sHeroLaser, mX - 8, mY - 9);
	}
}
