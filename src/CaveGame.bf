using System;
using SDL2;
using System.Collections;
using System.IO;
using System.Diagnostics;
using System.Interop;

namespace CaveGame;

static
{
	public static CaveGame gGame;

#if BF_PLATFORM_WASM

	public static int sProperWidth;
	public static int sProperHeight;

	[Export, CLink]
	static void onBrowserWindowResizeFirstStart(int newWidth, int newHeight) {
		sProperWidth = newWidth;
		sProperHeight = newHeight;
	}

	[Export, CLink]
	static void onBrowserWindowResize(int newWidth, int newHeight) {
		gGame.mWidth = newWidth;
		gGame.mHeight = newHeight;
		SDL.SetWindowSize(gGame.mWindow, newWidth, newHeight);
	}

	public static int sTouches = 0;
	struct xy {
		public int x;
		public int y;
	}
	public static xy[5] sTouch;

	[Export, CLink]
	static void onTouch(int touches, int touch0x, int touch0y, int touch1x, int touch1y, int touch2x, int touch2y, int touch3x, int touch3y, int touch4x, int touch4y) {
		sTouches = touches;
		sTouch[0].x = touch0x;
		sTouch[0].y = touch0y;
		sTouch[1].x = touch1x;
		sTouch[1].y = touch1y;
		sTouch[2].x = touch2x;
		sTouch[2].y = touch2y;
		sTouch[3].x = touch3x;
		sTouch[3].y = touch3y;
		sTouch[4].x = touch4x;
		sTouch[4].y = touch4y;
		gGame.MobilePressesParse();
	}
	public static uint sTouchParsed = 0;


#endif
	public static uint8 sFrameCounter = 0;
}

class CaveGame : SDLApp
{
	public List<Entity> mEntities = new List<Entity>() ~ DeleteContainerAndItems!(_);
	public Hero mHero;
	public int mScore;
	public float mDifficulty;
	public Random mRand = new Random() ~ delete _;
#if !NOTTF
	Font mFont ~ delete _;
#endif
	float mBkgPos;
	int mEmptyUpdates;
	bool mPaused;
	bool mDebugStringShow = true;
	bool mFullscreen = false;
	bool mKeyAltDown = false;

	bool mRecolored = false;
	Random mRandom = new Random();

	public TiledMap mTiledMap = new .() ~ delete _;

	public int mCameraX = 0;
	public int mCameraY = 0;
	public readonly int32 mZoom = 3;

	public var mNoclip = false;
	//public var mFly = false;

	public SDL.AudioDeviceID mAudioDeviceID = 0;

	public bool mKeyUp = false;
	public bool mKeyDown = false;
	public bool mKeyLeft = false;
	public bool mKeyRight = false;
	public bool mKeyDash = false;

	public this()
	{
		gGame = this;

		mHero = new Hero();
		AddEntity(mHero);

		//mHero.mX = 6 * 24 + mHero.mDrawOffsetX;
		mHero.mX = 6 * 24 + mHero.mDrawOffsetX;
		mHero.mY = 8 * 24 + mHero.mDrawOffsetY;

		//mHero.mX = 395.25f; mHero.mY = 102.5f; mHero.mSpeedX = -7f; mHero.mSpeedY = -6f;
	}

	public ~this()
	{
		Images.Dispose();
		Sounds.Dispose();
		delete mRandom;
	}

	private void MyInit()
	{
		mDidInit = true;

		/*String exePath = scope .();
		Environment.GetExecutableFilePath(exePath);
		String exeDir = scope .();
		if (Path.GetDirectoryPath(exePath, exeDir) case .Ok)
			Directory.SetCurrentDirectory(exeDir);*/

		SDL.Init(.Video | .Events | .Audio);
		SDL.EventState(.JoyAxisMotion, .Disable);
		SDL.EventState(.JoyBallMotion, .Disable);
		SDL.EventState(.JoyHatMotion, .Disable);
		SDL.EventState(.JoyButtonDown, .Disable);
		SDL.EventState(.JoyButtonUp, .Disable);
		SDL.EventState(.JoyDeviceAdded, .Disable);
		SDL.EventState(.JoyDeviceRemoved, .Disable);

#if BF_PLATFORM_WASM
		mWidth = sProperWidth;
		mHeight = sProperHeight;
#endif

		//mWindow = SDL.CreateWindow(mTitle, .Undefined, .Undefined, mWidth, mHeight, .Hidden); // Initially hide window
		mWindow = SDL.CreateWindow(mTitle, .Undefined, .Undefined, mWidth, mHeight, .Shown); // Initially hide window

		mRenderer = SDL.CreateRenderer(mWindow, -1, .Accelerated);
		mScreen = SDL.GetWindowSurface(mWindow);
		SDLImage.Init(.PNG);
		//mHasAudio = SDLMixer.OpenAudio(44100, SDLMixer.MIX_DEFAULT_FORMAT, 2, 4096) >= 0;
		//mHasAudio = SDLMixer.OpenAudio(48000, SDLMixer.MIX_DEFAULT_FORMAT, 2, 4096) >= 0;

		mHasAudio = true;

		SDL.SDL_AudioSpec desired = default;
		desired.freq = 44100;
		desired.format = SDLMixer.MIX_DEFAULT_FORMAT;
		desired.samples = 4096;

		mAudioDeviceID = SDL.OpenAudioDevice(null, 0, ref desired, let obtained, 0);
		Debug.WriteLine($"\tid: {(int)mAudioDeviceID} \n\tchannels: {obtained.channels} \n\tsize: {obtained.size} \n\tsamples: {obtained.samples}");

		/*for (int32 i in 0..<SDL.GetNumAudioDevices(0))
		{
		    let pName = SDL.GetAudioDeviceName(i, 0);
		    var name = StringView(pName);
		    Debug.WriteLine(name);

		    let id = SDL.[Friend]OpenAudioDevice(pName, 0, ref desired, let obtained, 0);
		    Debug.WriteLine($"\tid: {(int)id} \n\tchannels: {obtained.channels} \n\tsize: {obtained.size} \n\tsamples: {obtained.samples}");
		}*/
		Debug.WriteLine("DONE!");

		SDL.SetRenderDrawBlendMode(mRenderer, .Blend);

		SDLTTF.Init();
	}

	public override void Init()
	{
		MyInit();
		//base.Init();
		SDL.SetWindowResizable(mWindow, true);

		SDL.SetWindowTitle(mWindow, "Cave Game");

		mTiledMap.Parse();

		Images.Init();
		if (mHasAudio)
			Sounds.Init();

		mFont = new Font();
		//mFont.Load("zorque.ttf", 24);
		mFont.Load("images/Main.fnt", 0);



		//SDLMixer.PlayMusic(Sounds.mMusic, -1);

		//SDL.SetWindowFullscreen(mWindow, (.)SDL.WindowFlags.FullscreenDesktop);



	}



//	public static c_int on_canvassize_changed(c_int eventType, WebAssembly.EmscriptenFullscreenChangeEvent *event, void *userData)
//	{
//		//gGame.mWidth = event.elementWidth;
//		//gGame.mHeight = event.elementHeight;
//		//gGame.mWidth = event.screenWidth;
//
//		if (event.elementWidth != 0) {
//			gGame.mWidth = event.elementWidth;
//			gGame.mHeight = event.elementHeight;
//		} else if (event.screenWidth != 0) {
//			gGame.mWidth = event.screenWidth;
//			gGame.mHeight = event.screenHeight;
//		}
//
//	  	return 0;
//
//	}

	public void DrawString(float x, float y, String str, SDL.Color color, bool centerX = false)
	{
		DrawString(mFont, x, y, str, color, centerX);
	}

	public override void Draw()
	{
		//Draw(Images.sSpaceImage, 0, mBkgPos - 1024);
		//Draw(Images.sSpaceImage, 0, mBkgPos);

		mTiledMap.Draw();
		sFrameCounter += 1; //LATER: handle overflow

		if (SDL.GetQueuedAudioSize(mAudioDeviceID) <= 0) {
			Sounds.MusicPush();
		}

		for (var entity in mEntities)
			entity.Draw();

		//DrawString(8, 4, scope String()..AppendF("SCORE: {}", mScore), .(64, 255, 64, 255));

		if (mDebugStringShow) {
			//DrawString(mWidth / 2, 300, scope $"x is {mHero.mX}, y is {mHero.mY}", .(255, 255, 255, 255), true);
			DrawString( 8, 0, scope $"Y = {mHero.mY}", .(255, 255, 255, 255));
			DrawString( 8, 20, scope $"floor = {mHero.mTouchesBottom}", .(255, 255, 255, 255));
			DrawString( 8, 40, scope $"shift = {IsKeyDown(.LShift)}", .(255, 255, 255, 255));
			DrawString( 8, 140, scope $"sFrameCounter = {sFrameCounter}", .(255, 255, 255, 255));
#if BF_PLATFORM_WASM
			DrawString( 8, 60, scope $"sTouches = {sTouches}", .(255, 255, 255, 255));
			//DrawString( 8, 80, scope $"sTouch1x = {sTouch}", .(255, 255, 255, 255));
#endif

			//IsKeyDown(.LShift)

			//let (x,y) = mTiledMap.GetCoordinates(mHero.mX, mHero.mY);
			//DrawString(mWidth / 2, 300, scope $"x is {x}, y is {y}", .(255, 255, 255, 255), true);
		}

#if BF_PLATFORM_WASM
			MobilePressesDraw();
#endif
	}

#if BF_PLATFORM_WASM
	public void MobilePressesParse() {
		//Debug.WriteLine("still alive3");
		//Debug.WriteLine(scope $"{sTouches}");
		const float stepFromBorder = 2f;
		const float screenMiddle = (.)0x70/0xff;
		const float betweenButtons = (.)0x40/0xff;

		float width = mWidth - stepFromBorder - stepFromBorder;
		float widthScreenMiddle = width * screenMiddle;
		float widthBothButtons = (width - widthScreenMiddle) / 2;
		float widthBetweenButtons = widthBothButtons * betweenButtons;
		float widthButton = (widthBothButtons - widthBetweenButtons) / 2;

		uint temp = 0;

		for (int i = 0; i < sTouches; i++) {
			uint bit = 1;
			float x = stepFromBorder;
			float xprev = 0;
			readonly float tx = (.)sTouch[i].x;
			readonly float ty = (.)sTouch[i].y;
			void Step(float width) {
				xprev = x;
				x += width;
				if (xprev < tx && tx < x) {
					if (stepFromBorder < ty && ty < (gGame.mHeight - stepFromBorder) ) {
						temp = temp | bit;
					}
				}
				bit = bit * 2;
			}
			Step(widthButton);
			Step(widthBetweenButtons);
			Step(widthButton);
			Step(widthScreenMiddle);
			Step(widthButton);
			Step(widthBetweenButtons);
			Step(widthButton);
		}

		sTouchParsed = temp;

		//sTouchParsed = (1 << 5);

	}

	
	void MobilePressesDraw() {
		//Debug.WriteLine("still aliveDraw");
		const float stepFromBorder = 2f;
		const float screenMiddle = (.)0x70/0xff;
		const float betweenButtons = (.)0x40/0xff;

		float width = mWidth - stepFromBorder - stepFromBorder;
		float widthScreenMiddle = width * screenMiddle;
		float widthBothButtons = (width - widthScreenMiddle) / 2;
		float widthBetweenButtons = widthBothButtons * betweenButtons;
		float widthButton = (widthBothButtons - widthBetweenButtons) / 2;

		void White() {
			SDL.SetRenderDrawColor(mRenderer, 0xc0, 0xc0, 0xc0, 0x80);
		}

		void GreenBlue() {
			SDL.SetRenderDrawColor(mRenderer, 0x20, 0xc0, 0xc0, 0x80);
		}

		var rect = scope SDL.Rect(2, 2, 2, 2);
		rect.y = (.)stepFromBorder;

		uint tempTouchParsed = sTouchParsed;

		void Line(float width) {
			if (tempTouchParsed & 1 != 0) {
				GreenBlue();
			} else {
				White();
			}
			rect.w = (.)width;
			SDL.RenderFillRect(mRenderer, rect);
			rect.y = mHeight - (.)stepFromBorder;
			SDL.RenderFillRect(mRenderer, rect);
			rect.y = (.)stepFromBorder;
			rect.x += (.)width;
			tempTouchParsed = tempTouchParsed >> 1;
		}

		void Pass(float width) {
			rect.x += (.)width;
			tempTouchParsed = tempTouchParsed >> 1;
		}

		Line(widthButton);
		Line(widthBetweenButtons);
		Line(widthButton);
		Pass(widthScreenMiddle);
		Line(widthButton);
		Line(widthBetweenButtons);
		Line(widthButton);
	}
#endif

	public void ExplodeAt(float x, float y, float sizeScale, float speedScale)
	{
		let explosion = new Explosion();
		explosion.mSizeScale = sizeScale;
		explosion.mSpeedScale = speedScale;
		explosion.mX = x;
		explosion.mY = y;
		mEntities.Add(explosion);
	}

	public void AddEntity(Entity entity)
	{
		mEntities.Add(entity);
	}

	public override void KeyDown(SDL.KeyboardEvent evt)
	{
		base.KeyDown(evt);
		if (evt.keysym.sym == .P)
			mPaused = !mPaused;
		else if (evt.keysym.sym == .LALT || evt.keysym.sym == .RALT)
			mKeyAltDown = true;
		else if ((mKeyAltDown && evt.keysym.sym == .RETURN) || evt.keysym.sym == .F) {
			mFullscreen = ! mFullscreen;
			if (mFullscreen) {
				SDL.SetWindowFullscreen(mWindow, (.)SDL.WindowFlags.FullscreenDesktop);
			} else {
				SDL.SetWindowFullscreen(mWindow, 0);
			}

		} else if (evt.keysym.sym == .V) {
//#if BF_PLATFORM_WASM
//			//https://github.com/emscripten-core/emscripten/blob/main/system/include/emscripten/html5.h
//
//			WebAssembly.EmscriptenFullscreenStrategy s = .();
//			s.scaleMode = .Stretch;
//			s.canvasResolutionScaleMode = .Hidef; //hi def
//			s.filteringMode = .Nearest; //default
//
//			s.canvasResizedCallback = => on_canvassize_changed;
//			WebAssembly.emscripten_enter_soft_fullscreen("canvas", &s);
//#endif

		} else if (evt.keysym.sym == .Q || evt.keysym.sym == .ESCAPE) {
			SDL.Event newEvent;
			newEvent.type = .Quit;
			SDL.PushEvent(ref newEvent);
		}

		/*if (!mFullscreen) {
			mFullscreen = true;
			SDL.SetWindowFullscreen(mWindow, (.)SDL.WindowFlags.FullscreenDesktop);
		}*/
	}

	public override void KeyUp(SDL.KeyboardEvent evt)
	{
		base.KeyUp(evt);
		if (evt.keysym.sym == .LALT || evt.keysym.sym == .RALT)
			mKeyAltDown = false;
	}

	public override void HandleEvent(SDL.Event evt)
	{

		base.HandleEvent(evt);
		if (evt.type == .WindowEvent && evt.window.windowEvent == .Resized) {
			mWidth = evt.window.data1;
			mHeight = evt.window.data2;
		}

		else if (evt.type == .MouseButtonDown || evt.type == .MouseMotion) {

		}
	}

	public override void Update()
	{
		if (mPaused)
			return;

		base.Update();

		HandleInputs();
		//SpawnEnemies();

		// Make the game harder over time
		mDifficulty += 0.0001f;

		// Scroll the background
		mBkgPos += 0.6f;
		if (mBkgPos > 1024)
			mBkgPos -= 1024;

		for (var entity in mEntities)
		{
			entity.mCounter++;
			entity.Update();
			if (entity.mIsDeleting)
			{
				// '@entity' refers to the enumerator itself
                @entity.Remove();
				delete entity;
			}
		}
	}

	void HandleInputs()
	{
		mKeyDown = IsKeyDown(.Down) || IsKeyDown(.S);
		mKeyUp = IsKeyDown(.Up) || IsKeyDown(.W) || IsKeyDown(.Z);
		mKeyLeft = IsKeyDown(.Left) || IsKeyDown(.A);
		mKeyRight = IsKeyDown(.Right) || IsKeyDown(.D);
		mKeyDash = IsKeyDown(.LShift);

		if (IsKeyDown(.Space))
		{
			mHero.mX = 6 * 24 + mHero.mDrawOffsetX;
			mHero.mY = 8 * 24 + mHero.mDrawOffsetY;
		}

#if BF_PLATFORM_WASM
		mKeyLeft |= sTouchParsed & (1 << 0) != 0;
		mKeyLeft |= sTouchParsed & (1 << 1) != 0;
		mKeyRight |= sTouchParsed & (1 << 1) != 0;
		mKeyRight |= sTouchParsed & (1 << 2) != 0;
		mKeyUp |= sTouchParsed & (1 << 5) != 0;
		mKeyUp |= sTouchParsed & (1 << 6) != 0;
#endif
	}

	void SpawnEnemies()
	{
		bool hasEnemies = false;
		for (var entity in mEntities)
			if (entity is Enemy)
				hasEnemies = true;
		if (hasEnemies)
			mEmptyUpdates = 0;
		else
			mEmptyUpdates++;

		float spawnScale = 0.4f + (mEmptyUpdates * 0.025f);
		spawnScale += mDifficulty;

		if (mRand.NextDouble() < 0.002f * spawnScale)
			SpawnSkirmisher();

		if (mRand.NextDouble() < 0.0005f * spawnScale)
			SpawnGoliath();
	}

	void SpawnSkirmisher()
	{
		let spawner = new EnemySkirmisher.Spawner();
		spawner.mLeftSide = mRand.NextDouble() < 0.5;
		spawner.mY = ((float)mRand.NextDouble() * 0.5f + 0.25f) * mHeight;
		AddEntity(spawner);
	}

	void SpawnGoliath()
	{
		let enemy = new EnemyGolaith();
		enemy.mX = ((float)mRand.NextDouble() * 0.5f + 0.25f) * mWidth;
		enemy.mY = -300;
		AddEntity(enemy);
	}
}
