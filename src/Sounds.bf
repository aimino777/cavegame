using System;
using SDL2;
using System.Collections;
using Libopenmpt;
using System.IO;

//using System.Diagnostics;

namespace CaveGame;

static class Sounds
{
	public static Sound sBeepMed;
	public static Sound sBeepHigh;
	public static Sound sBeepHighLong;
	public static Sound sFail;
	public static Sound sLaser;
	public static Sound sExplode;
	public static bool sbMusicQuitted;
	public static bool sbMusicPlaying;

	public static SDLMixer.Music* mMusic;

	static List<Sound> sSounds = new .() ~ delete _;

	public static void DisposeGen() {
		//Debug.WriteLine("disposed");

		sbMusicQuitted = true;
		delete soundbuf;
		delete buffer;
		Libopenmpt.openmpt_module_destroy(mod);

	}

	public static void Dispose()
	{
		ClearAndDeleteItems(sSounds);

		if (!sbMusicPlaying) {
			DisposeGen();
		}

		
	}

	public static Result<Sound> Load(StringView fileName)
	{
		Sound sound = new Sound();
		if (sound.Load(fileName) case .Err)
		{
			delete sound;
			return .Err;
		}
		sSounds.Add(sound);
		return sound;
	}


	public static Result<void> Init()
	{
		/*sBeepMed = Try!(Load("sounds/beep_med.wav"));
		sBeepHigh = Try!(Load("sounds/beep_high.wav"));
		sBeepHighLong = Try!(Load("sounds/beep_high_long.wav"));
		sFail = Try!(Load("sounds/fail.wav"));
		sLaser = Try!(Load("sounds/laser01.wav"));
		sExplode = Try!(Load("sounds/explode01.wav"));*/

		//let initedFlags = SDLMixer.Init(.Mod);

		mMusic = null;
		//mMusic = SDLMixer.LoadMUS("music/qwertyy_-_Reiterate_Lap_2_Revised.mp3");
		//mMusic = SDLMixer.LoadMUS("music/a_day_like_tomorrow.mod");

		//SDLMixer.PlayMusic(Sounds.mMusic, -1);

		musicInit();

		return .Ok;
	}

	public static int soundbuflen = 4096*16;
	public static int soundpos = 0;
	public static var soundbuf = new int16[soundbuflen];
	public static Libopenmpt.openmpt_module* mod;

	public static List<uint8> buffer = new .();

	public static void musicInit() {

		//File.ReadAll("music/a_bit_of_8_bit_reality_by_rimbi6000.it", buffer);
		File.ReadAll("music/a_bit_of_8_bit_reality_by_rimbi6000.it", buffer);
		mod = Libopenmpt.openmpt_module_create_from_memory2(buffer.Ptr, (int32)buffer.Count, null, null, null, null, null, null, null);
		Libopenmpt.openmpt_module_ctl_set_text(mod, "play.at_end", "continue"); //TODO, copy in separate memory

		//SDLMixer.HookMusic(=> MusicCallback, null);
	}

	public static void MusicPush() {
		return;
		if (sbMusicQuitted) return;
		sbMusicPlaying = true;

		let samplelen = soundbuflen/4;
		let samplesWritten = Libopenmpt.openmpt_module_read_interleaved_stereo(mod, 44100, (.)samplelen, (.)soundbuf.Ptr);
		if (samplesWritten != samplelen){
			//SDLMixer.HookMusic(null, null);
		}

		SDL.QueueAudio(gGame.mAudioDeviceID, (.)soundbuf.Ptr, (.)soundbuflen);
		SDL.PauseAudioDevice((.)gGame.mAudioDeviceID, 0);

		sbMusicPlaying = false;
		if (sbMusicQuitted) {
			DisposeGen();
		}
	}

	public static void MusicCallback(void* ignored_udata, uint8* const_stream, int32 const_len) {
		//TODO: do proper multithreading here, it still bugs sometimes
		//Debug.WriteLine("play +");
		if (sbMusicQuitted) return;
		sbMusicPlaying = true;

		let samplelen = const_len/4;
		let samplesWritten = Libopenmpt.openmpt_module_read_interleaved_stereo(mod, 48000, samplelen, (.)const_stream);
		if (samplesWritten != samplelen){
			//SDLMixer.HookMusic(null, null);
		}

		sbMusicPlaying = false;
		if (sbMusicQuitted) {
			DisposeGen();
		}
	}
}


