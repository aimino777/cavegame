Module.onRuntimeInitialized = function() {
	//alert("initialized")
	
	window.onerror = function(msg, url, line, col, error) {
		// https://stackoverflow.com/questions/951791/javascript-global-event-mechanism
		// Note that col & error are new to the HTML 5 spec and may not be 
		// supported in every browser.  It worked for me in Chrome.
		var extra = !col ? '' : '\ncolumn: ' + col;
		extra += !error ? '' : '\nerror: ' + error;

		// You can view the information in an alert to see things working like this:
		alert("Error: " + msg + "\nurl: " + url + "\nline: " + line + extra);

		// TODO: Report this error via ajax so you can keep track
		//       of what pages have JS issues

		var suppressErrorAlert = true;
		// If you return true, then error alerts (like in older versions of 
		// Internet Explorer) will be suppressed.
		return suppressErrorAlert;
	};
	
	//https://developer.mozilla.org/en-US/docs/Web/API/Touch_events
	//document.addEventListener("touchstart", handleStart);
	var onBrowserWindowResize = Module.cwrap('onBrowserWindowResize', 'null', ['number', 'number']);
	var onBrowserWindowResizeFirstStart = Module.cwrap('onBrowserWindowResizeFirstStart', 'null', ['number', 'number']);
	
	
	onBrowserWindowResizeFirstStart(window.innerWidth, window.innerHeight)
	
	window.onresize = function(e){
		onBrowserWindowResize(window.innerWidth, window.innerHeight);
	}
	
	//alert("first start");
	
	var onTouch = Module.cwrap('onTouch', 'null', ['number',
		'number','number','number','number','number','number','number','number','number','number']);
		
	function jsOnTouch(e) {
		e.preventDefault();
		var touches = e.touches.length;
		var x0=0, y0=0, x1=0, y1=0, x2=0, y2=0, x3=0, y3=0, x4=0, y4=0;
		if (touches > 0) {
			x0 = e.touches[0].clientX;
			y0 = e.touches[0].clientY;
			if (touches > 1) {
				x1 = e.touches[1].clientX;
				y1 = e.touches[1].clientY;
				if (touches > 2) {
					x2 = e.touches[2].clientX;
					y2 = e.touches[2].clientY;
					if (touches > 3) {
						x3 = e.touches[3].clientX;
						y3 = e.touches[3].clientY;
						if (touches > 4) {
							x4 = e.touches[4].clientX;
							y4 = e.touches[4].clientY;
						}
					}
				}
			}
		}
		onTouch(touches, x0, y0, x1, y1, x2, y2, x3, y3, x4, y4);
	}
	
	function jsOnTouchEnd(e) {
		e.preventDefault();
		onTouch(0);
	}
	
	// https://www.youtube.com/watch?v=TaPdgj8mucI
	//touchmove
	//touchend
	document.addEventListener('touchstart', jsOnTouch);
	
	document.addEventListener('touchmove',  jsOnTouch);
	
	document.addEventListener('touchend',  jsOnTouchEnd);
	
	document.addEventListener('touchcancel', jsOnTouchEnd);
};


